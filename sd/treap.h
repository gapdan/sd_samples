//
// Created by gapdan on 5/12/2017.
//

#ifndef TREAP_TREAP_H
#define TREAP_TREAP_H
#include <cstring>
#include <queue>
#include <iostream>

template<typename T> class TreapNode {
private:
    // Pointeri catre fiul stanga, fiul dreapta si parinte.


public:
    // Pointer catre parinte.
    TreapNode<T> *parent;
    // Informatia utila.
    T info;
    TreapNode<T> *left_son, *right_son;
    int prio;

    // Functie ce compara 2 elemente de tipul T si intoarce < 0, = 0, > 0, daca primul
    // element este mai mic ca, egal cu, mai mare ca al doilea element.
    int (*compare)(T, T);

    // Constructor.
    TreapNode(int (*compare) (T, T), T info, int p) {
        left_son = right_son = parent = NULL;
        this->compare = compare;
        memcpy(&(this->info), &info, sizeof(T));
        // Choose a random priority.
        //prio = rand();
        prio = p;
    }

    // Insereaza informatia in arbore.
    virtual TreapNode<T>* insert_info(T x, int p) {
        int next_son;
        TreapNode<T>* result;

        if (compare(x, info) <= 0)
            next_son = 0;
        else
            next_son = 1;

        if (next_son == 0) {  // left son
            if (left_son == NULL) {
                left_son = new TreapNode<T>(compare, x, p);
                left_son->parent = this;
                left_son->push_up();
                result = left_son;
            } else
                result = left_son->insert_info(x, p);
        } else { // right son
            if (right_son == NULL) {
                right_son = new TreapNode<T>(compare, x, p);
                right_son->parent = this;
                right_son->push_up();
                result = right_son;
            } else
                result = right_son->insert_info(x, p);
        }
        return result;
    }

    void rotate_right() {
        TreapNode<T>* gparent = parent->parent;

        TreapNode<T>* rson = right_son;
        parent->left_son = rson;
        if (rson != NULL)
            rson->parent = parent;
        right_son = parent;

        parent->parent = this;

        if (gparent != NULL) {
            if (gparent->left_son == parent)
                gparent->left_son = this;
            else
                gparent->right_son = this;
        }
        parent = gparent;
    }

    void rotate_left() {
        TreapNode<T>* gparent = parent->parent;

        TreapNode<T> *lson = left_son;
        parent->right_son = lson;
        if (lson != NULL)
            lson->parent = parent;
        left_son = parent;

        parent->parent = this;

        if (gparent != NULL) {
            if (gparent->left_son == parent)
                gparent->left_son = this;
            else
                gparent->right_son = this;
        }
        parent = gparent;
    }

    void push_up() {
        while (parent != NULL) {
            if (prio > parent->prio) {
                if (parent->left_son == this) {
                    rotate_right();
                } else {
                    rotate_left();
                }
            } else
                break;
        }
    }

    void push_down() {
        while (left_son!=NULL || right_son!=NULL) {
            // rotim in directia fiului cu prioritate mai mare
            // pt ca avem un max-heap
            if (left_son==NULL)
                right_son->rotate_left();
            else if (right_son==NULL)
                left_son->rotate_right();
            else // avem ambii fii
            if (left_son->prio < right_son->prio)
                right_son->rotate_left();
            else
                left_son->rotate_right();
        }
    }

    // Intoarce un pointer catre nodul ce contine informatia si NULL altfel.
    virtual TreapNode<T>* find_info(T x) {
        if (compare(x, info) == 0)
            return this;

        if (compare(x, info) <= 0) {
            if (left_son != NULL)
                return left_son->find_info(x);
            else
                return NULL;
        } else {
            if (right_son != NULL)
                return right_son->find_info(x);
            else
                return NULL;
        }
    }

    // Sterge un element din arbore.
    // Intoarce 1 daca e stearsa radacina arborelui si 0 altfel.
    virtual int remove_info(T x) {
        //if (x==7751)
        TreapNode<T> *t = find_info(x);
        if (t != NULL)
            return t->remove_self();
        else
            return 0;
    }

    virtual int remove_self() {
        TreapNode<T> *p;
        if (left_son == NULL && right_son == NULL) {
            if (parent == NULL) { // this == root
                delete this;
                return 1;
            } else {
                if (parent->left_son == this)
                    parent->left_son = NULL;
                else
                    parent->right_son = NULL;
                delete this;
            }
        } else {
            if (left_son != NULL) {
                // Gaseste nodul cu cea mai mare val din subarb stang.
                p = left_son;
                while (p->right_son != NULL)
                    p = p->right_son;
            } else { // right_son != NULL
                // Gaseste nodul cu cea mai mica val din subarb drept.
                p = right_son;
                while (p->left_son != NULL)
                    p = p->left_son;
            }
            memcpy(&info, &(p->info), sizeof(T));
            p->remove_self();
            push_down();
        }
        return 0;
    }
};

// T = tipul elementelor stocate in arbore.
// TreeNodeType = tipul nodurilor arborelui (de ex., TreapNode<T>
// sau alte tipuri de noduri care implementeaza comportamente diferite).
template<typename T, typename TreeNodeType> class Treap {
public:
    TreeNodeType* root;
    int (*compare) (T, T);

    Treap(int (*compare) (T, T)) {
        root = NULL;
        this->compare = compare;
    }

    // Insereaza un element nou in arbore.
    virtual void insert_info(T x, int p) {
        if (root == NULL)
            root = new TreeNodeType(compare, x, p);
        else {
            root->insert_info(x, p);
        }
        while (root->parent!=NULL)
            root = root->parent;
    }

    // Cauta un element in arbore.
    virtual TreeNodeType* find_info(T x) {
        if (root == NULL)
            return NULL;
        else
            return root->find_info(x);
    }

    // Sterge un element din arbore.
    virtual void remove_info(T x) {
        if (root != NULL) {
            if (root->remove_info(x)) {
                root = NULL;
            }
        }
        if (root!=NULL)
            while (root->parent!=NULL)
                root = root->parent;
    }

    bool check(){
        TreeNodeType* p;
        std::queue<TreeNodeType*> q;
        q.push(root);
        bool ok = true;
        while (!q.empty()) {
            p = q.front();
            q.pop();
            if(p -> left_son != NULL) {
                if (p->left_son->prio > p->prio) return false;
                q.push(p->left_son);
            }
            if(p -> right_son != NULL) {
                if (p->right_son->prio > p->prio) return false;
                q.push(p->right_son);
            }
        }
        return true;
    }
};


#endif //TREAP_TREAP_H
