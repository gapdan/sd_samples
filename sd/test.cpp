#include <iostream>
#include "treap.h"

int cmp(int a, int b) {
    if(a == b) return 0;
    if(a < b) return -1;
        else return 1;
}
int main() {
    std::cout << "Hello, World!" << std::endl;
    Treap<int, TreapNode<int> >T(cmp);
    T.insert_info(10, 50);
    T.insert_info(5, 31);
    T.insert_info(15, 2);
    T.insert_info(2, 7);
    T.insert_info(7, 3);
    T.remove_info(10);
    if (T.check() == true) std::cout<<"E ok\n";
        else std::cout<<"Nu e ok\n";
    return 0;
}